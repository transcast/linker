use actix_web::HttpResponse;
use actix_web::{get, web, Error};
use diesel::{RunQueryDsl, QueryDsl};
use crate::models::RecordingPointer;
use super::super::DbPool;

use super::super::schema;

#[get("/recordings")]
pub async fn get_recordings(
    pool: web::Data<DbPool>,
) -> Result<HttpResponse, Error> {
    let pointers = web::block(move || {
        let conn = pool.get()?;
        let pointers = schema::recording_pointer::table
            .limit(50)
            .load::<RecordingPointer>(&conn)
            .expect("fetching recording pointers failed");
        Ok::<Vec<RecordingPointer>, anyhow::Error>(pointers)
    })
        .await?
        .map_err(actix_web::error::ErrorInternalServerError)?;
    Ok(HttpResponse::Ok().json(pointers))
}
