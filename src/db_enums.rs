// for some reason they can't be in models.rs
use diesel_derive_enum::DbEnum;
use serde::Serialize;

#[derive(DbEnum, Serialize, Debug)]
#[serde(rename_all = "snake_case")]
#[DieselType = "Data_source"]
pub enum DataSource {
    /// https://musicbrainz.org/
    Musicbrainz,
    /// https://discogs.com/
    Discogs,
    
    /// https://spotify.com/
    Spotify,
    /// https://tidal.com/
    Tidal,
    /// https://music.apple.com/
    AppleMusic,
    /// https://deezer.com/
    Deezer,
    /// https://music.amazon.com/
    AmazonMusic,
    /// https://pandora.com/
    Pandora,
    
    /// https://youtube.com/
    Youtube,
    /// https://soundcloud.com/
    Soundcloud,
    /// https://bandcamp.com/
    Bandcamp,
    
    /// https://last.fm/
    Lastfm,
    /// https://genius.com/
    Genius,
    
    /// https://rutracker.org/
    Rutracker,
    /// https://nyaa.si/
    NyaaSi,
    /// https://thepiratebay.org/
    Thepiratebay,
    
    /// https://lnk.to/
    Linkfire,
    /// https://ffm.to/
    FeatureFm,
    /// https://smarturl.it/
    Smarturl,

    /// Sometimes we don't know/handle a provider yet, but it seems useful to save that for the future
    Unknown,
}
