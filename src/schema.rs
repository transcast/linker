table! {
    use diesel::sql_types::*;
    use crate::db_enums::*;

    recording_data (id) {
        id -> Varchar,
        pointer -> Varchar,
        title -> Varchar,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::db_enums::*;

    recording_pointer (id) {
        id -> Varchar,
        external_id -> Varchar,
        provider -> Data_source,
        url -> Varchar,
    }
}

joinable!(recording_data -> recording_pointer (pointer));

allow_tables_to_appear_in_same_query!(
    recording_data,
    recording_pointer,
);
