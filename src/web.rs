#[macro_use]
extern crate diesel;
extern crate dotenv;

use diesel::r2d2::{self, ConnectionManager};
use diesel::pg::PgConnection;
use actix_web::{web, middleware, App, HttpServer};
use log::info;
pub mod db_enums;
pub mod models;
pub mod schema;
pub mod controllers;
use crate::controllers::recordings::get_recordings;

type DbPool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    dotenv::dotenv().ok();

    // set up database connection pool
    let connspec = std::env::var("DATABASE_URL").expect("DATABASE_URL");
    let manager = ConnectionManager::<PgConnection>::new(connspec);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");

    let bind = "127.0.0.1:3434";

    info!("Starting server at: {}", &bind);

    // Start HTTP server
    HttpServer::new(move || {
        App::new()
            // set up DB pool to be used with web::Data<Pool> extractor
            .app_data(web::Data::new(pool.clone()))
            .wrap(middleware::Logger::default())
            .service(get_recordings)
    })
    .bind(&bind)?
    .run()
    .await
}
