#[macro_use]
extern crate diesel;
extern crate dotenv;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;

mod db_enums;
mod models;
mod providers;
mod schema;
use crate::db_enums::DataSource;
use crate::models::NewRecordingPointer;

pub fn establish_postgres_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

fn main() {
    use schema::recording_pointer;
    let connection = establish_postgres_connection();
    let new_recording_pointer = NewRecordingPointer {
        external_id: "e5d2c049-589d-4ac6-bb41-a155a57da0b6",
        provider: &DataSource::Musicbrainz,
        url: "https://musicbrainz.org/recording/e5d2c049-589d-4ac6-bb41-a155a57da0b6",
    };
    println!("{:?}", new_recording_pointer);
    let rows_aff = diesel::insert_into(recording_pointer::table)
        .values(&new_recording_pointer)
        .on_conflict_do_nothing()
        .execute(&connection)
        .expect("db insert failed");
    println!("rows affected: {}", rows_aff);
}
