use diesel::Queryable;
use serde::Serialize;
use super::db_enums::DataSource;

#[derive(Queryable, Serialize, Debug)]
/// Points to a recording entity in specified provider
pub struct RecordingPointer {
    pub id: String,
    /// Format depends on provider.
    /// Intended to reduce possible duplicates.
    /// * If the ID is obvious from the URL
    ///     (like https://beta.musicbrainz.org/recording/efc14538-08f8-4d1b-9fbd-c0067b1812cc,
    ///     https://open.spotify.com/track/1OAp6qN5KmoGUQ2edICKsC,
    ///     https://www.youtube.com/watch?v=KushW6zvazM),
    ///     and no other data is required to fetch the data, it's just a plain ID
    /// * If the ID also requires other recording data for reference
    ///     (e.g. https://music.apple.com/album/ghost-rule/1147144714?i=1147145175),
    ///     it's specific to the provider
    /// * If the ID is not in the URL at all
    ///     (e.g. https://genius.com/Deco27-ghost-rule-lyrics,
    ///     https://www.last.fm/music/DECO*27/_/Ghost+Rule),
    ///     it's probably the URL part
    pub external_id: String,
    pub provider: DataSource,
    pub url: String,
}

use super::schema::recording_pointer;

#[derive(Insertable, Debug)]
#[table_name="recording_pointer"]
pub struct NewRecordingPointer<'a> {
    pub external_id: &'a str,
    pub provider: &'a DataSource,
    pub url: &'a str,
}

#[derive(Queryable, Serialize, Debug)]
pub struct RecordingData {
    pub id: String,
    pub pointer: RecordingPointer,
    pub title: String,
}
