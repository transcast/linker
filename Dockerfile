FROM rust:bullseye as build

WORKDIR /app

RUN cargo install diesel_cli

RUN mkdir src && echo 'fn main() {}' > src/main.rs

COPY Cargo.toml .
COPY Cargo.lock .

# installs dependencies and removes shit, to leverage caching
RUN cargo build --release && rm -rf src/*.rs

COPY src src

RUN cargo build --bin main --target "$(uname -m)"-unknown-linux-gnu

RUN ln -s /app/target/"$(uname -m)"-unknown-linux-gnu /app/target/current
RUN ls -al /app/target/current/debug

CMD ["/app/target/current/debug/main"]

FROM debian:bullseye-slim

WORKDIR /app

RUN \
    DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y libpq5

COPY --from=build /app/target/current/debug/main linker

CMD ["./linker"]
