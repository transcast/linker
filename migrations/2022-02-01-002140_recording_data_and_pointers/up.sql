CREATE TYPE data_source AS ENUM (
    'musicbrainz',
    'discogs',

    'spotify',
    'tidal',
    'apple_music',
    'deezer',
    'amazon_music',
    'pandora',

    'youtube',
    'soundcloud',
    'bandcamp',

    'lastfm',
    'genius',

    'rutracker',
    'nyaa_si',
    'thepiratebay',

    'linkfire',
    'feature_fm',
    'smarturl',

    'unknown'
);

CREATE TABLE recording_pointer (
    id VARCHAR(36) PRIMARY KEY DEFAULT gen_random_uuid()::text,
    external_id VARCHAR(36) NOT NULL,
    provider data_source NOT NULL,
    url VARCHAR(256) NOT NULL
);
CREATE UNIQUE INDEX recording_pointer_providers_id ON recording_pointer (external_id) INCLUDE (provider);

CREATE TABLE recording_data (
    id VARCHAR(36) PRIMARY KEY DEFAULT gen_random_uuid()::text,
    pointer VARCHAR(36) REFERENCES recording_pointer(id) NOT NULL,
    title VARCHAR(64) NOT NULL
);
CREATE INDEX recording_data_pointer ON recording_data (pointer);
